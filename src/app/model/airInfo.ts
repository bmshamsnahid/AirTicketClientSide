export class AirInfo {
  constructor() {}
  _id: string;
  name: string;
  description: string;
  sourceLocationId: string;
  destinationLocationId: string;
  airplaneId: string;
  fare: string;
  dates: [string];
  time: string;
}
