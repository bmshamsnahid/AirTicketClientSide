import {AirInfo} from './airInfo';
import {Airplane} from './airplane';
import {Seat} from './seat';
import {Location} from './location';

export class AirDetail {
  constructor() {}
  _id: string;
  airInfo: AirInfo;
  airplane: Airplane;
  seats: Seat[];
  sourceLocation: Location;
  destinationLocation: Location;
}
