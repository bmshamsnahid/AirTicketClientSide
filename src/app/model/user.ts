export class User {
  constructor() {}
  _id: string;
  email: string;
  name: string;
  password: string;
  createdDate: string;
  isAdmin: boolean;
}
