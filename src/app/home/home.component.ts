import { Component, OnInit } from '@angular/core';
import {AirInfo} from '../model/airInfo';
import {Airplane} from '../model/airplane';
import {Location} from '../model/location';
import {AirplaneService} from '../services/airplane.service';
import {LocationService} from '../services/location.service';
import {AirInfoService} from '../services/air-info.service';
import {AirDetail} from '../model/airDetail';
import {Seat} from '../model/seat';
import {SeatService} from '../services/seat.service';
import {BookSeat} from '../model/bookSeat';
import {User} from '../model/user';
import {BookSeatService} from '../services/book-seat.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  currentUserObj: any;
  currentUser: User;

  airInfo: AirInfo;

  airDetails: AirDetail[];

  selectedAirInfo: AirInfo;

  noOfEconomyClassSeats: number;
  noOfBusinessClassSeats: number;

  name: string;
  description: string;
  sourceLocationId: string;
  destinationLocationId: string;
  airplaneId: string;
  fare: string;
  dates: [string];
  time: string;

  airplanes: Airplane[];
  locations: Location[];

  seats: Seat[];

  constructor(private airplaneService: AirplaneService,
              private locationService: LocationService,
              private airInfoService: AirInfoService,
              private seatService: SeatService,
              private bookSeatService: BookSeatService) {
    this.selectedAirInfo = new AirInfo();
    this.airInfo = new AirInfo();

    this.currentUserObj = JSON.parse(localStorage.getItem('currentUserObj'));
    if (this.currentUserObj) {
      this.currentUser = this.currentUserObj.currentUser;
    }

    this.airInfoService.getAirDetails()
      .subscribe((response) => {
        this.airDetails = response.data;
        console.log(this.airDetails);
      });
  }

  ngOnInit() {
    this.airplaneService.getAirplanes()
      .subscribe((response) => {
        if (response.success) {
          this.airplanes = response.data;
        }
      });
    this.locationService.getLocations()
      .subscribe((response) => {
        if (response.success) {
          this.locations = response.data;
        }
      });
  }

  onClickSearchAirInfo() {
    if (!this.sourceLocationId || !this.destinationLocationId || !this.dates) {
      console.log('Invalid data.');
    } else {
      this.airDetails = this.airDetails.filter((airDetail: AirDetail) => {
        if (airDetail.airInfo.dates === this.dates &&
          airDetail.sourceLocation._id === this.sourceLocationId &&
          airDetail.destinationLocation._id === this.destinationLocationId) {
            return airDetail;
        }
      });
    }
  }

  onClickTable(airInfo) {
    this.selectedAirInfo = airInfo;
    this.seatService.getAirplaneSeatsById(this.selectedAirInfo.airplaneId)
      .subscribe((response) => {
        this.seats = response.data;
        console.log(this.seats);
      });
  }

  onSelectSource(location: Location) {
    this.sourceLocationId = location._id;
  }

  onSelectDestination(location: Location) {
    this.destinationLocationId = location._id;
  }

  onClickBookSeat (seat: Seat) {
    const bookSeat: BookSeat = new BookSeat();
    bookSeat.seatId = seat._id;
    bookSeat.airInfoId = this.selectedAirInfo._id;
    bookSeat.userId = this.currentUser._id;
    this.bookSeatService.createBookSeat(bookSeat)
      .subscribe((response) => {
        if (response.success) {
          seat.isBooked = true;
          this.seatService.updateSeat(seat)
            .subscribe((response2) => {
              if (response2.success) {
                console.log('Seat Book');
              }
            });
        }
      });
  }

  onClickUpdateAirInfo() {
    this.airInfoService.updateAirInfo(this.selectedAirInfo)
      .subscribe((response) => {
        console.log(response);
        if (response.success) {
          console.log('Updated');
        }
      });
  }

}
