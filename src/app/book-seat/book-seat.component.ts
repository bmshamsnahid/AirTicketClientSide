import { Component, OnInit } from '@angular/core';
import {Seat} from '../model/seat';
import {SeatService} from '../services/seat.service';
import {Airplane} from '../model/airplane';
import {AirplaneService} from '../services/airplane.service';
import {BookSeatService} from '../services/book-seat.service';
import {BookSeat} from '../model/bookSeat';
import {User} from '../model/user';

@Component({
  selector: 'app-book-seat',
  templateUrl: './book-seat.component.html',
  styleUrls: ['./book-seat.component.css']
})
export class BookSeatComponent implements OnInit {

  airplanes: Airplane[];
  airplane: Airplane;
  selectAirplane: Airplane;
  seats: Seat[];
  bookSeats: BookSeat[];
  userBookSeats: Seat[];
  dataReceived: boolean;
  userSeatsId: String[] = [];

  currentUserObj: any;
  currentUser: User;

  constructor(private seatService: SeatService,
              private bookSeatService: BookSeatService) {
    this.dataReceived = false;

    this.currentUserObj = JSON.parse(localStorage.getItem('currentUserObj'));
    if (this.currentUserObj) {
      this.currentUser = this.currentUserObj.currentUser;
    }

    this.seatService.getAllSeats()
      .subscribe((response) => {
        if (response.success) {
          this.seats = response.data;
          this.bookSeatService.getAllBookSeat()
            .subscribe((response2) => {
              if (response2.success) {
                this.bookSeats = response2.data;
                this.bookSeats = this.bookSeats.filter((bookSeat: BookSeat) => {
                  if (bookSeat.userId === this.currentUser._id) {
                    this.userSeatsId.push(bookSeat.seatId);
                    return bookSeat;
                  }
                });
                this.seats = this.seats.filter((seat: Seat) => {
                  return (this.userSeatsId.includes(seat._id) === true) ? seat: null;
                });
                console.log(this.userSeatsId);
                console.log(this.seats);
                this.dataReceived = true;
              }
            });
        }
      });
  }

  ngOnInit() {
    this.seatService.getAllSeats()
      .subscribe((response) => {
        if (response.success) {
          this.seats = response.data;
          this.dataReceived = true;
        }
      });
  }

  onClickUnBookSeat(seat: Seat) {
    seat.isBooked = false;
    this.seatService.updateSeat(seat)
      .subscribe((response) => {
        if (response.success) {
          console.log('Seat un booked');
        }
      });
  }


}
