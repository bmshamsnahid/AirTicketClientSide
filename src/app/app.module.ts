import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { SignInComponent } from './sign-in/sign-in.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { LogoutComponent } from './logout/logout.component';
import { HomeComponent } from './home/home.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {RouterModule} from '@angular/router';
import { NavigationComponent } from './navigation/navigation.component';
import { BookSeatComponent } from './book-seat/book-seat.component';
import {AirInfoService} from './services/air-info.service';
import {AirplaneService} from './services/airplane.service';
import {AuthService} from './services/auth.service';
import {AuthGuardService} from './services/auth-guard.service';
import {LocationService} from './services/location.service';
import {SeatService} from './services/seat.service';
import {UserService} from './services/user.service';
import {BookSeatService} from './services/book-seat.service';


@NgModule({
  declarations: [
    AppComponent,
    SignInComponent,
    SignUpComponent,
    LogoutComponent,
    HomeComponent,
    NavigationComponent,
    BookSeatComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    RouterModule.forRoot([
      { path: 'home', canActivate: [ AuthGuardService ],  component: HomeComponent },
      { path: 'sign-in', component: SignInComponent },
      { path: 'seats', canActivate: [ AuthGuardService ], component: BookSeatComponent },
      { path: 'sign-up', component: SignUpComponent },
      { path: 'logout', canActivate: [ AuthGuardService ], component: LogoutComponent },
      { path: '', redirectTo: 'sign-in', pathMatch: 'full' },
      { path: '**', redirectTo: 'sign-in', pathMatch: 'full' }
    ])
  ],
  providers: [AirInfoService, AirplaneService, AuthService, AuthGuardService, LocationService, SeatService, UserService, BookSeatService],
  bootstrap: [AppComponent]
})
export class AppModule { }
