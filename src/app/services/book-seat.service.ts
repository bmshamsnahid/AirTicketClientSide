import { Injectable } from '@angular/core';
import {Headers, Http, RequestOptions, Response} from '@angular/http';
import {User} from '../model/user';
import {environment} from '../../environments/environment';
import {BookSeat} from '../model/bookSeat';

@Injectable()
export class BookSeatService {

  headers = new Headers();
  options = new RequestOptions();
  currentUserObj: any;
  currentUser: User;
  token: string;

  constructor(private http: Http) {
    this.currentUserObj = JSON.parse(localStorage.getItem('currentUserObj'));
    if (this.currentUserObj) {
      this.currentUser = this.currentUserObj.currentUser;
      this.token = this.currentUserObj.token;
      this.headers.append('Authorization', this.token);
    }
    this.headers.append('Content-Type', 'application/json');
    this.options = new RequestOptions({ headers: this.headers });
  }

  createBookSeat(bookSeat: BookSeat) {
    return this.http.post(`${environment.baseUrl}/api/bookSeat`, JSON.stringify(bookSeat), this.options)
      .map((response: Response) => {
        if (response.json().success) {
          return response.json();
        } else if (response.json().message) {
          console.log(response.json().message);
        } else {
          console.log('Fatal Server Error');
        }
      });
  }

  getAllBookSeat() {
    return this.http.get(`${environment.baseUrl}/api/bookSeat`, this.options)
      .map((response: Response) => {
        if (response.json().success) {
          return response.json();
        } else if (response.json().message) {
          console.log(response.json().message);
        } else {
          console.log('Fatal Server Error');
        }
      });
  }

  updateBookSeat(bookSeat: BookSeat) {
    return this.http.patch(`${environment.baseUrl}/api/bookSeat/${bookSeat._id}`, JSON.stringify(bookSeat), this.options)
      .map((response: Response) => {
        if (response.json().success) {
          return response.json();
        } else if (response.json().message) {
          console.log(response.json().message);
        } else {
          console.log('Fatal Server Error');
        }
      });
  }

  deleteBookSeat(bookSeat: BookSeat) {
    return this.http.delete(`${environment.baseUrl}/api/bookSeat/${bookSeat._id}`, this.options)
      .map((response: Response) => {
        if (response.json().success) {
          return response.json();
        } else if (response.json().message) {
          console.log(response.json().message);
        } else {
          console.log('Fatal Server Error');
        }
      });
  }

}
