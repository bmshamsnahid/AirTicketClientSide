import { Injectable } from '@angular/core';
import {Headers, Http, RequestOptions, Response} from '@angular/http';
import {User} from '../model/user';
import {environment} from '../../environments/environment';
import {Location} from '../model/location';

@Injectable()
export class LocationService {

  headers = new Headers();
  options = new RequestOptions();
  currentUserObj: any;
  currentUser: User;
  token: string;

  constructor(private http: Http) {
    this.currentUserObj = JSON.parse(localStorage.getItem('currentUserObj'));
    if (this.currentUserObj) {
      this.currentUser = this.currentUserObj.currentUser;
      this.token = this.currentUserObj.token;
      this.headers.append('Authorization', this.token);
    }
    this.headers.append('Content-Type', 'application/json');
    this.options = new RequestOptions({ headers: this.headers });
  }

  createLocation (location: Location) {
    return this.http.post(`${environment.baseUrl}/api/location`, JSON.stringify(location), this.options)
      .map((response: Response) => {
        if (response.json().success) {
          return response.json();
        } else if (response.json().message) {
          console.log(response.json().message);
        } else {
          console.log('Fatal Server Error');
        }
      });
  }

  getLocations () {
    return this.http.get(`${environment.baseUrl}/api/location`, this.options)
      .map((response: Response) => {
        if (response.json().success) {
          return response.json();
        } else if (response.json().message) {
          console.log(response.json().message);
        } else {
          console.log('Fatal Server Error');
        }
      });
  }

  updateLocation (location: Location) {
    return this.http.patch(`${environment.baseUrl}/api/location/${location._id}`, JSON.stringify(location), this.options)
      .map((response: Response) => {
        if (response.json().success) {
          return response.json();
        } else if (response.json().message) {
          console.log(response.json().message);
        } else {
          console.log('Fatal Server Error');
        }
      });
  }

  deleteLocation (location: Location) {
    return this.http.delete(`${environment.baseUrl}/api/location/${location._id}`, this.options)
      .map((response: Response) => {
        if (response.json().success) {
          return response.json();
        } else if (response.json().message) {
          console.log(response.json().message);
        } else {
          console.log('Fatal Server Error');
        }
      });
  }

}
